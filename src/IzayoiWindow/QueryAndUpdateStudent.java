package IzayoiWindow;

import java.awt.List;
import java.sql.*;  

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;  

public class QueryAndUpdateStudent {
	Connection ct;   
    PreparedStatement ps,ps2;  
    ResultSet rs,rs2;  
    String user,pwd;  
    //将连接数据库的方法封装为一个方法  
    public void ConnectSQL()  
    {  
        try {  
            Class.forName("com.mysql.jdbc.Driver"); //加载驱动  
            ct = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/student?user=root&password=709414&useSSL=false");
              
            System.out.println("已成功连接数据库...");  
              
        } catch (Exception e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        }  
    }  
    
    public JTable QueryStudentBasicInfo(String sid, String name)
    {
    	String[] columns = {"学号","姓名","性别","入学年龄","入学年份","班级"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
    	try {
    		if(name.length() == 0 && sid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Students where sid = ?");
    			ps.setString(1, sid);
    		}
    		else if(name.length() != 0 && sid.length() == 0){
    			System.out.println("2");
    			ps=ct.prepareStatement("select * from Students where sname = ?");
    			ps.setString(1, name);
    		}
    		else{
    			System.out.println(sid.length());
    			System.out.println(name);
    			ps=ct.prepareStatement("select * from Students where sid = ?");
    			ps.setString(1, sid);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
    						rs.getString(5),rs.getString(6)});
    			}
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该学生", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;  
    }

	public JTable QueryStuCoursesInfo(String sid, String name) {
		// TODO Auto-generated method stub
		String[] columns = {"学号","姓名","课程编号","课程名称","入学年份","成绩"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
    	try {
    		if(name.length() == 0 && sid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select studentsid, sname, courseid,cname, selectYear, grade from SelectInfo,"
    					+ " Students,Courses where studentsid = sid and sid = ? and courseid = cid");
    			ps.setString(1, sid);
    		}
    		else if(name.length() != 0 && sid.length() == 0){
    			System.out.println("2");
    			ps=ct.prepareStatement("select studentsid, sname, courseid,cname, selectYear, grade from SelectInfo, "
    					+ "Students,Courses where studentsid = sid and sname = ? and courseid = cid");
    			ps.setString(1, name);
    		}
    		else{
    			System.out.println(sid.length());
    			System.out.println(name);
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,cname, cprofessor, selectYear, "
    					+ "grade from SelectInfo, Students,Courses where studentsid = sid and sid = ? and courseid = cid");
    			ps.setString(1, sid);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
    						rs.getString(5),rs.getString(6)});
    			}
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该学生的选课信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;  
	}

	public JTable QueryStuGPA(String sid, String name, String cid, String cname) {
		// TODO Auto-generated method stub
		String[] columns = {"学号","姓名","课程编号","课程名称","教课老师","选课时间","成绩"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
    	try {
    		if(sid.length() != 0 && cid.length() != 0){
    			System.out.println(sid.length());
    			System.out.println(name);
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,cname, cprofessor, selectYear, grade from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and sid = ? and cid = ? and cid = courseid");
    			ps.setString(1, sid);
    			ps.setString(2, cid);
    		}
    		else if(name.length() != 0 && cid.length() != 0){
    			System.out.println("2");
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,cname, cprofessor, selectYear, grade from SelectInfo, Students,Courses"
    					+ " where studentsid = sid and sname = ? and cid = ? and cid = courseid");
    			ps.setString(1, name);
    			ps.setString(2, cid);
    		}
    		else if(sid.length() != 0 && cname.length() != 0){
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,cname, cprofessor, selectYear, grade from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and sid = ? and cname = ? and cid = courseid");
    			ps.setString(1, sid);
    			ps.setString(2, cname);
    		}
    		else if(name.length() != 0 && cname.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,cname, cprofessor, selectYear, grade from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and sname = ? and cname = ? and cid = courseid");
    			ps.setString(1, name);
    			ps.setString(2, cname);
    		}
    		else{
    			JOptionPane.showMessageDialog(null, "请至少输入 学号或姓名 和 课程编号或课程名称！", "提示信息", JOptionPane.WARNING_MESSAGE);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
    						rs.getString(5),rs.getString(6),rs.getString(7)});
    			}
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该学生的选课信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table; 
	}

	public JTable QueryStuCourseGrade(String sid) {
		// TODO Auto-generated method stub
		String[] columns = {"学号","姓名","性别","入学年龄","入学年份","班级","加权"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
    	try {
    		ps=ct.prepareStatement("select distinct studentsid, sname, sgender,sage, sregister, sclass, grade, cscore from SelectInfo, Students,Courses "
    				+ "where studentsid = sid and sid = ? and cid = courseid;");
    		ps.setString(1, sid);
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	String temp = null;
            	String[] sb = {"1","2","3","4","5","6","7"};
            	sb[0] = rs.getString(1);
            	sb[1] = rs.getString(2);
            	sb[2] = rs.getString(3);
            	sb[3] = rs.getString(4);
            	sb[4] = rs.getString(5);
            	sb[5] = rs.getString(6);
            	sb[6] = temp;
            	rs.previous();
            	double ans = 0;
            	int cnt = 0;      	
    			while(rs.next()){
    				if(rs.getInt(7) == 0){
    					continue;
    				}
    				ans += (rs.getInt(7)*rs.getInt(8));
    				cnt += rs.getInt(8);
    			}
    			ans = ans/cnt;
    			String answer = Double.toString(ans);
    			sb[6] = answer;
    			model.addRow(sb);
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该学生的选课信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;
	}

	public JTable QueryCouBasciInfo(String cid, String cname) {
		// TODO Auto-generated method stub
		String[] columns = {"课程编号","课程名称","教课教授","学分","取消时间","选课年级"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
    	try {
    		if(cname.length() == 0 && cid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Courses where cid = ?");
    			ps.setString(1, cid);
    		}
    		else if(cname.length() != 0 && cid.length() == 0){
    			System.out.println("2");
    			ps=ct.prepareStatement("select * from Courses where cname = ?");
    			ps.setString(1, cname);
    		}
    		else{
    			System.out.println(cid.length());
    			System.out.println(cname);
    			ps=ct.prepareStatement("select * from Courses where cid = ?");
    			ps.setString(1, cid);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
    						rs.getString(5),rs.getString(6)});
    			}
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该课信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;  
	}

	public JTable QueryCouSituation(String cid, String cname) {
		// TODO Auto-generated method stub
		String[] columns = {"选课学生学号","选课学生姓名","课程编号","课程名称","教课教授","学分","取消时间","选课年级"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
    	try {
    		if(cname.length() == 0 && cid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,selectYear cname, cprofessor, cscore, ccancelYear, cfitGrade "
    					+ "from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and cid = ? and cid = courseid");
    			ps.setString(1, cid);
    		}
    		else if(cname.length() != 0 && cid.length() == 0){
    			System.out.println("2");
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,selectYear cname, cprofessor, cscore, ccancelYear, cfitGrade "
    					+ "from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and cname = ? and cid = courseid");
    			ps.setString(1, cname);
    		}
    		else{
    			System.out.println(cid.length());
    			System.out.println(cname);
    			ps=ct.prepareStatement("select distinct studentsid, sname, courseid,selectYear cname, cprofessor, cscore, ccancelYear, cfitGrade "
    					+ "from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and cid = ? and cid = courseid");
    			ps.setString(1, cid);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
    						rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8)});
    			}
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table; 
	}

	public JTable QueryCourseGPA(String cid) {
		// TODO Auto-generated method stub
		String[] columns = {"课程编号","平均成绩","不及格","60－69","70－79","80－89","90－99","满分"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
        double avgGrade = 0;
        int cnt = 0;
        int fail = 0,from60 = 0, from70 = 0, from80 = 0, from90 = 0, oneHundred = 0;
    	try {
    		if(cid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select distinct studentsid, courseid, grade from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and cid = ? and cid = courseid");
    			ps.setString(1, cid);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				int tmp = rs.getInt(3);
    				if(tmp != 0){
    					avgGrade += tmp; cnt++;
    					if(tmp < 60){
    						fail++;
    					}
    					else if(tmp < 70){
    						from60++;
    					}
    					else if(tmp < 80){
    						from70++;
    					}
    					else if(tmp < 90){
    						from80++;
    					}
    					else if(tmp < 100){
    						from90++;
    					}
    					else{
    						oneHundred++;
    					}
    				}
    			}
    			avgGrade = avgGrade/cnt;
    			model.addRow(new String[]{cid,String.valueOf(avgGrade),String.valueOf(fail),String.valueOf(from60)
    					,String.valueOf(from70),String.valueOf(from80),String.valueOf(from90),String.valueOf(oneHundred)});
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table; 
	}
	

	public JTable QueryClassGPA(String classid) {
		// TODO Auto-generated method stub
		String[] columns = {"班级","平均加权"};
        DefaultTableModel model = new DefaultTableModel(null, columns);
        JTable table = null;
        double avgGrade = 0;
        int cnt = 0;
    	try {
    		if(classid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select distinct studentsid, courseid, cscore, grade from SelectInfo, Students,Courses"
    					+ " where studentsid = sid and sclass = ? and cid = courseid");
    			ps.setString(1, classid);
    		}
    		rs=ps.executeQuery(); 
            if(rs.next()){
            	rs.previous();
    			while(rs.next()){
    				int tmp = rs.getInt(3),tmp2 = rs.getInt(4);
    				if(tmp2 != 0){
    					avgGrade += tmp2 * tmp;
    					cnt += tmp;
    				}
    			}
    			avgGrade = avgGrade/cnt;
    			model.addRow(new String[]{classid,String.valueOf(avgGrade)});
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
            table = new JTable(model);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table; 
	}

	public void DeleteStuBasicInfo(String sid) {
		// TODO Auto-generated method stub
    	try {
    		if(sid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Students where sid = ?");
    			ps.setString(1, sid);
    			rs=ps.executeQuery(); 
   
    	
	            if(rs.next()){
	            	ps=ct.prepareStatement("delete from Students where sid = ?");
	    			ps.setString(1, sid);
	    			ps.executeUpdate(); 
	    			JOptionPane.showMessageDialog(null, "删除成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
	    		}
	           	else{
	           		JOptionPane.showMessageDialog(null, "未查询到该学生", "提示信息", JOptionPane.WARNING_MESSAGE); 
	           	}   
    		}
    		else{
    			JOptionPane.showMessageDialog(null, "请填入信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void DeleteCouBasicInfo(String cid) {
		// TODO Auto-generated method stub
    	try {
    		if(cid.length() != 0){
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Courses where cid = ?");
    			ps.setString(1, cid);
    			rs=ps.executeQuery();
   
            if(rs.next()){
            	ps=ct.prepareStatement("delete from Courses where cid = ?");
    			ps.setString(1, cid);
    			ps.executeUpdate(); 
    			JOptionPane.showMessageDialog(null, "删除成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该课程", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}  
    		}
    		else{
    			JOptionPane.showMessageDialog(null, "请输入信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void DeleteStuCoursesInfo(String sid, String cid) {
		// TODO Auto-generated method stub
    	try {
    		if(cid.length() == 0 || sid.length() == 0){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and sid = ? and cid = ? and cid = courseid");
    			ps.setString(1, sid);
    			ps.setString(2, cid);
    			rs=ps.executeQuery(); 
    		
            if(rs.next()){
            	ps=ct.prepareStatement("delete from SelectInfo where studentsid = ? and courseid = ?;");
            	ps.setString(1, sid);
    			ps.setString(2, cid);
    			ps.executeUpdate(); 
    			JOptionPane.showMessageDialog(null, "删除成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该课程", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void UpdateStuBasicInfo(String sid, String sname, int flag, String sage, String sregister, String sclass) {
		// TODO Auto-generated method stub
		try {
    		if(sid.length() != 10 || sname.length() == 0 || flag == 0 || sage.length() == 0 || sregister.length() == 0 || sclass.length() == 0 || Integer.valueOf(sage) < 10 || Integer.valueOf(sage) > 50 ){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			String hasaki = "";
    			if(flag == 1){
    				hasaki = "male";
    			}
    			else{
    				hasaki = "female";
    			}
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Students where sid = ?");
    			ps.setString(1, sid);
    			rs=ps.executeQuery(); 
    		
            if(rs.next()){
            	ps=ct.prepareStatement("UPDATE Students SET sname = ?, sgender = ?, sage = ? , "
            			+ "sregister = ?, sclass = ? where sid = ?");
            	ps.setString(1, sname);
    			ps.setString(2, hasaki);
    			ps.setString(3, sage);
    			ps.setString(4, sregister);
    			ps.setString(5, sclass);
    			ps.setString(6, sid);
    			ps.executeUpdate(); 
    			JOptionPane.showMessageDialog(null, "修改成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该学生", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void UpdateCouBasicInfo(String cid, String cname, String cpro, String cscore, String ccancel, String cfit) {
		// TODO Auto-generated method stub
		try {
    		if(cid.length() != 7 || cname.length() == 0 || cpro.length() == 0 || cscore.length() == 0 || cfit.length() == 0){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			String flag = null;
    			if(ccancel.length() != 0){
    				flag = ccancel;
    			}
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Courses where cid = ?");
    			ps.setString(1, cid);
    			rs=ps.executeQuery(); 
    		
            if(rs.next()){
            	ps=ct.prepareStatement("update Courses SET cname = ?, cprofessor = ?, cscore = ? , "
            			+ "ccancelYear = ?, cfitGrade = ? where cid = ? ");
            	ps.setString(1, cname);
    			ps.setString(2, cpro);
    			ps.setString(3, cscore);
    			ps.setString(4, flag);
    			ps.setString(5, cfit);
    			ps.setString(6, cid);
    			ps.executeUpdate(); 
    			JOptionPane.showMessageDialog(null, "修改成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该课程", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void UpdateStuCoursesInfo(String sid, String cid, String sy, String grade) {
		// TODO Auto-generated method stub
		try {
    		if(cid.length() != 7 || sid.length() != 10 || sy.length() == 0){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			String flag = null;
    			if(grade.length() != 0){
    				flag = grade;
    			}
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from SelectInfo, Students,Courses "
    					+ "where studentsid = sid and sid = ? and cid = ? and cid = courseid;");
    			ps.setString(1, sid);
    			ps.setString(2, cid);
    			rs=ps.executeQuery(); 
    		
            if(rs.next()){
            	ps=ct.prepareStatement("update SelectInfo SET selectYear = ? , grade = ? "
            			+ "where studentsid = ? and courseid = ?");
            	ps.setString(1, sy);
    			ps.setString(2, flag);
    			ps.setString(3, sid);
    			ps.setString(4, cid);
    			ps.executeUpdate(); 
    			JOptionPane.showMessageDialog(null, "修改成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "未查询到该选课信息", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void InsertStuBasicInfo(String sid, String sname, int flag, String sage, String sregister, String sclass) {
		// TODO Auto-generated method stub
		try {
    		if(sid.length() != 10 || sname.length() == 0 || flag == 0 || sage.length() == 0 || sregister.length() == 0 || sclass.length() == 0 || Integer.valueOf(sage) < 10 || Integer.valueOf(sage) > 50 ){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			String hasaki = "";
    			if(flag == 1){
    				hasaki = "male";
    			}
    			else{
    				hasaki = "female";
    			}
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Students where sid = ?");
    			ps.setString(1, sid);
    			rs=ps.executeQuery(); 
    		
            if(rs.next()){
            	JOptionPane.showMessageDialog(null, "该学生已存在", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
           	else{
           		ps=ct.prepareStatement("Insert into Students values(?, ?, ?, ?, ?, ?)");
           		ps.setString(1, sid);
            	ps.setString(2, sname);
    			ps.setString(3, hasaki);
    			ps.setString(4, sage);
    			ps.setString(5, sregister);
    			ps.setString(6, sclass);
 
    			ps.execute(); 
    			JOptionPane.showMessageDialog(null, "插入成功", "提示信息", JOptionPane.WARNING_MESSAGE);
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void InsertCouBasicInfo(String cid, String cname, String cpro, String cscore, String ccancel, String cfit) {
		// TODO Auto-generated method stub
		try {
    		if(cid.length() != 7 || cname.length() == 0 || cpro.length() == 0 || cscore.length() == 0 || cfit.length() == 0){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			String flag = null;
    			if(ccancel.length() != 0){
    				flag = ccancel;
    			}
    			System.out.println("1");
    			ps=ct.prepareStatement("select * from Courses where cid = ?");
    			ps.setString(1, cid);
    			rs=ps.executeQuery(); 
    		
            if(rs.next()){ 
    			JOptionPane.showMessageDialog(null, "该课程已存在", "提示信息", JOptionPane.WARNING_MESSAGE);
    		}
           	else{
           		ps=ct.prepareStatement("Insert into Courses values(?, ?, ?, ?, ?, ?)");
           		ps.setString(1, cid);
            	ps.setString(2, cname);
    			ps.setString(3, cpro);
    			ps.setString(4, cscore);
    			ps.setString(5, flag);
    			ps.setString(6, cfit);
    			
    			ps.execute(); 
    			JOptionPane.showMessageDialog(null, "插入成功", "提示信息", JOptionPane.WARNING_MESSAGE);
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void InsertStuCoursesInfo(String sid, String cid, String sy, String grade) {
		// TODO Auto-generated method stub
		try {
    		if(cid.length() != 7 || sid.length() != 10 || sy.length() == 0){
    			JOptionPane.showMessageDialog(null, "请填写正确的信息！", "提示信息", JOptionPane.WARNING_MESSAGE); 
    		}
    		else{
    			String flag = null;
    			if(grade.length() != 0){
    				flag = grade;
    			}
    			System.out.println("1");
    			ps=ct.prepareStatement("select sregister from students where sid = ?");
    			ps.setString(1, sid);
    			rs=ps.executeQuery(); 
    			ps2=ct.prepareStatement("select ccancelYear,cfitGrade from Courses where cid = ?");
    			ps2.setString(1, cid);
    			rs2=ps2.executeQuery(); 
            if(rs.next() && rs2.next()){
            	System.out.println(rs.getInt(1));
            	System.out.println(rs2.getInt(2));
            	System.out.println(rs2.getString(1));
            	if((2017 - rs.getInt(1)) >= rs2.getInt(2) && (rs2.getString(1) == null || rs2.getInt(1) > rs.getInt(1))){
	            	ps=ct.prepareStatement("Insert into SelectInfo values(?,?,?,?)");
	            	
	            	ps.setString(1, sid);
	    			ps.setString(2, cid);
	    			ps.setString(3, sy);
	    			ps.setString(4, flag);
	    			ps.execute(); 
	    			JOptionPane.showMessageDialog(null, "添加成功", "提示信息", JOptionPane.WARNING_MESSAGE); 
    			}
            	else{
            		JOptionPane.showMessageDialog(null, "添加失败", "提示信息", JOptionPane.WARNING_MESSAGE); 
            	}
    		}
           	else{
           		JOptionPane.showMessageDialog(null, "请确认输入信息是否正确", "提示信息", JOptionPane.WARNING_MESSAGE); 
           	}   
    		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
