package IzayoiWindow;

import java.awt.event.*;  
import java.awt.*;  
  
import javax.swing.*;  
  
/* 
 * 注册界面。 
 */  
class studentRegister extends JFrame implements ActionListener{  
  
    //定义组件  
    JFrame jf;  
    JPanel jp;  
    JLabel jl1,jl2,jl4;  
    JTextField jtf1,jtf2,jtf4;  
    JButton jb1,jb2;
    JRadioButton radio1,radio2 = null;
    int flag = 0;
    public studentRegister(int flag)  
    {  
        //初始化组件  
    	this.flag = flag;
        jf=new JFrame();  
        jp=new JPanel();  
        jl1=new JLabel("请输入用户名：");  
        jtf1=new JTextField(10);  
        jtf1.setToolTipText("用户名必须为6到20位的字母_或者数字");  
        jl2=new JLabel("请输入密码：");  
        jtf2=new JTextField(10);  
        jtf2.setToolTipText("密码必须为6到10位的字母_或者数字");  
        jl4=new JLabel("请输入学号：");  
        jtf4=new JTextField(10);  
        jtf4.setToolTipText("学号必须为10位数字");  
          
        ButtonGroup group = new ButtonGroup(); 
        radio1 = new JRadioButton("学生注册");
        radio2 = new JRadioButton("教师注册");
        radio1.addActionListener(this);
        radio2.addActionListener(this);
        group.add(radio1);
        group.add(radio2);
        
        jb1=new JButton("返回");  
        jb1.setToolTipText("点我返回登录界面哦");  
        jb2=new JButton("注册");  
        jb1.addActionListener(this);  
        jb2.addActionListener(this);  
          
        jp.setLayout(new GridLayout(5,2));  
          
        jp.add(jl1);  
        jp.add(jtf1);  
          
        jp.add(jl2);  
        jp.add(jtf2); 
          
        jp.add(jl4);  
        jp.add(jtf4);  
        
        jp.add(radio1);
        jp.add(radio2); 
        
        jp.add(jb1);  
        jp.add(jb2);  
             
        this.add(jp);  
        this.setTitle("注册界面");  
        this.setBounds(200, 100, 250, 150);  
        this.setVisible(true);  
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
//      this.setResizable(false);  
          
          
          
    }  
      
      
      
      
  
    public void actionPerformed(ActionEvent e) {  
      
        if(e.getActionCommand()=="返回")  
        {  
            this.dispose();  
            //new studentLogin();  
//          System.out.println("-------");  
              
        }
        else if(e.getActionCommand()=="注册")  
        {  
                //调用注册方法  
            this.zhuce(flag);  
              
        } 
        else if(e.getSource() == radio1)//学生登陆
        {
        	flag = 1;
        }
        else if(e.getSource() == radio2)//老师登陆
        {
        	flag = 2;
        }
          
    }  
   public void zhuce(int flag)  
   {  
       	String regex1="\\w{1,20}"; //用户名必须是6到20位  
        boolean flag1=jtf1.getText().matches(regex1);  
          
        String regex2="\\w{6,10}"; //密码必须是6到10位  
        boolean flag2=jtf2.getText().matches(regex2);  
          
        //String regex3="[\\u4e00-\\u9fa5]{2,4}"; //姓名必须是汉字2-4个字  
        //boolean flag3=jtf3.getText().matches(regex3);  
          
        String regex4="\\d{10,10}";  //学号必须是3-6位  
        boolean flag4=jtf4.getText().matches(regex4);  
          
//      if(jtf1.getText()==null||jtf2.getText()==null||jtf3.getText()==null||jtf4.getText()==null)  
        if(flag1==false)  
        {  
            JOptionPane.showMessageDialog(null, "用户名填写错误,必须为1-20位字母_或者数字", "提示信息", JOptionPane.WARNING_MESSAGE);  
            jtf1.setText("");  
        }
        if(flag2==false)  
        {  
            JOptionPane.showMessageDialog(null, "密码填写错误,必须为6-10位字母_或者数字", "提示信息", JOptionPane.WARNING_MESSAGE);  
            jtf2.setText("");  
        }
        if(flag4==false)  
        {  
            JOptionPane.showMessageDialog(null, "学号填写错误,必须为10位数字", "提示信息", JOptionPane.WARNING_MESSAGE);  
            jtf4.setText("");  
        }
        else if(flag1 == true && flag2 == true && flag4 == true)
        {             
            //调用注册方法/先检查要注册的用户名是否存在  
             SQLserver ss=new SQLserver();  
             ss.ConnectSQL();  
             ss.ZhuceVerify(jtf4.getText(),flag,jtf1.getText(),jtf2.getText());  
            
//          ss.UserRegis(jtf1.getText(),jtf2.getText(),jtf3.getText(), jtf4.getText());  
            this.jtf1.setText("");  
            this.jtf2.setText("");  
            //this.jtf3.setText("");  
            this.jtf4.setText("");  
              
        }  
   }  
      
}  