package IzayoiWindow;

import javax.swing.*;  

import java.awt.*;  
import java.awt.event.*;  
import java.sql.*;  
  
public class studentLogin extends JFrame implements ActionListener{  
      
    //定义登录界面的组件  
    JButton jb1,jb2,jb3=null;  
    JRadioButton jrb1,jrb2=null;  
    JPanel jp1,jp2,jp3,jp4=null;  
    JTextField jtf=null;  
    JLabel jlb1,jlb2=null;  
    JPasswordField jpf=null;  
    JRadioButton radio1,radio2 = null;
    ButtonGroup group = null;
    int flag = 0;
    
    public static void main(String[] args)  
    {  
        studentLogin ur=new studentLogin();  
    }  
      
    public studentLogin()  
    {  
        //创建组件  
         //创建组件  
        jb1=new JButton("登录");  
        jb2=new JButton("注册");  
        jb3=new JButton("退出");  
        //设置监听  
        jb1.addActionListener(this);  
        jb2.addActionListener(this);  
        jb3.addActionListener(this);  
        
        ButtonGroup group = new ButtonGroup(); 
        radio1 = new JRadioButton("学生登陆");
        radio2 = new JRadioButton("教师登陆");
        radio1.addActionListener(this);
        radio2.addActionListener(this);
        
        jlb1=new JLabel("用户账号：");  
        jlb2=new JLabel("密   码：");  
          
        jtf=new JTextField(10);  
        jpf=new JPasswordField(10);  
          
        jp1=new JPanel();  
        jp2=new JPanel();  
        jp3=new JPanel();  
        jp4=new JPanel();
          
        jp1.add(jlb1);  
        jp1.add(jtf);  
          
        jp2.add(jlb2);  
        jp2.add(jpf);  
          
        jp3.add(jb1);  
        jp3.add(jb2);  
        jp3.add(jb3);  
        
        group.add(radio1);
        group.add(radio2);
        jp4.add(radio1);
        jp4.add(radio2);
        
        this.add(jp1);  
        this.add(jp2);  
        this.add(jp4);  
        this.add(jp3);
        
        this.setVisible(true);  
        this.setResizable(false);  
        this.setTitle("注册登录界面");  
        this.setLayout(new GridLayout(4,1));  
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        this.setBounds(300, 200, 300, 180);  
          
          
    }  
  
    @Override  
    public void actionPerformed(ActionEvent e) {  
          
        //监听各个按钮  
        if(e.getActionCommand()=="退出")  
        {  
            System.exit(0);  
        }
        else if(e.getActionCommand()=="登录")  
        {  
            //调用登录方法  
            this.login();  
        }
        else if(e.getActionCommand()=="注册")  
        {  
            //调用注册方法  
            this.Regis();  
        }  
        else if(e.getSource() == radio1)//学生登陆
        {
        	flag = 1;
        }
        else if(e.getSource() == radio2)//老师登陆
        {
        	flag = 2;
        }
    }  
      
    //注册方法  
     public void Regis() {  

         //this.dispose();  //关闭当前界面  
         new studentRegister(flag);   //打开新界面  
           
    }  
  
    //登录方法  
	public void login() {  
          
        SQLserver s = new SQLserver();  
        s.ConnectSQL();  
        //System.out.println(radio1.getText());
        s.SQLverify(jtf.getText(), String.valueOf(jpf.getPassword()), flag);  
        this.jtf.setText("");  
        this.jpf.setText("");  
        flag = 0;
    }  
  
}  