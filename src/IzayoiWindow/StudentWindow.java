package IzayoiWindow;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.*;
import java.sql.ResultSet;

public class StudentWindow extends JFrame implements ActionListener{
	
	int flag = 0;
	JMenu StudentInfo, GradeInfo, CoursesInfo, InsertInfo;
	JMenuItem stuBasicInfo, stuCoursesInfo, stuGPA, stuCourseGrade, classGPA, courseGPA, couBasciInfo, couSituation;
	JMenuBar all;
	JPanel jsid,jname;
	JPanel jsid2,jname2;
	JPanel jsid4;
	JPanel jsid3,jname3,jcid1,jcname1;
	JPanel jcid5,jcname5;
	JPanel jcid6,jcname6;
	JPanel jcid7,jcname7;
	JPanel jclassid7;
	JPanel jsid9;
	JPanel jcid10;
	JPanel jcid11,jsid11;
	ResultSet result;
	
	JButton stuBasicInfoa,stuBasicInfob,stuBasicInfoc,stuBasicInfod,stuBasicInfoe;
	JButton stuCoursesInfoa,stuCoursesInfob,stuCoursesInfoc,stuCoursesInfod,stuCoursesInfoe;
	JButton stuGPAa,stuGPAb,stuGPAc,stuGPAd;
	JButton stuCourseGradea,stuCourseGradeb,stuCourseGradec,stuCourseGraded;
	
	JButton classGPAa,classGPAb,classGPAc;
	JButton courseGPAa,courseGPAb,courseGPAc;
	JButton couBasciInfoa,couBasciInfob, couBasciInfoc,couBasciInfod,couBasciInfoe;
	JButton couSituationa,couSituationb,couSituationc,couSituationd;

	JButton dstuBasicInfoa,dstuBasicInfob;
	JButton dcouBasicInfoa,dcouBasicInfob;
	JButton dstuCoursesInfoa,dstuCoursesInfob;
	
	JButton male,female;
	ButtonGroup groupsb;
	
	JFrame JF,SBIFA;
	JLabel stuSid,stuName;
	JTextField sid,name;
	
	JFrame JF2,SCIF;
	JLabel stuSid2,stuName2,courseid,coursename;
	JTextField sid2,name2,cid1,cname1;
	
	JFrame JF3,SG;
	JLabel stuSid3,stuName3;
	JTextField sid3,name3;
	
	JFrame JF4,SCG;
	JLabel stuSid4;
	JTextField sid4;
	
	JFrame JF5,CBIF;
	JLabel cid5,cname5;
	JTextField cidText5,cnameText5;
	
	JFrame JF6,CSA;
	JLabel cid6,cname6;
	JTextField cidText6,cnameText6;
	
	JFrame JF7,CG;
	JLabel cid7,cname7;
	JTextField cidText7,cnameText7;
	
	JFrame JF8,CG2;
	JLabel classid8;
	JTextField classidText8;
	
	JFrame JF9;
	JLabel stuSid9;
	JTextField sid9;
	
	JFrame JF10;
	JLabel couCid10;
	JTextField cid10;
	
	JFrame JF11;
	JLabel couCid11,stuSid11;
	JTextField cid11,sid11;
	
	JFrame JF12;
	JLabel stuSid12,stuName12,stuGender12,stuAge12,stuRegister12,stuClass12;
	JTextField sid12,sname12,sage12,sregister12,sclass12;
	JButton uStuBasicInfoa,uStuBasicInfob;
	JPanel jsid12,jname12,jgender12,jage12,jregister12,jclass12;
	
	JFrame JF13;
	JLabel couCid13,couName13,couPro13,couScore13,couCancel13,couFit13;
	JTextField cid13,cname13,cpro13,cscore13,ccancel13,cfit13;
	JButton uCouBasicInfoa,uCouBasicInfob;
	JPanel jcid13,jcname13,jcpro13,jcscore13,jccancel13,jcfit13;
	
	JFrame JF14;
	JLabel studentsid14,courseid14,selectYear14,grade14;
	JTextField ssid14,cid14,sy14,g14;
	JButton uStuCoursesInfoa,uStuCoursesInfob;
	JPanel jssid14,jcid14,jsy14,jgrade14;
	
	JMenuItem InsertStuInfo,InsertCouInfo,InsertSelInfo;
	
	JFrame JF15;
	JLabel stuSid15,stuName15,stuGender15,stuAge15,stuRegister15,stuClass15;
	JTextField sid15,sname15,sage15,sregister15,sclass15;
	JButton iStuBasicInfoa,iStuBasicInfob;
	JPanel jsid15,jname15,jgender15,jage15,jregister15,jclass15;
	
	JFrame JF16;
	JLabel couCid16,couName16,couPro16,couScore16,couCancel16,couFit16;
	JTextField cid16,cname16,cpro16,cscore16,ccancel16,cfit16;
	JButton iCouBasicInfoa,iCouBasicInfob;
	JPanel jcid16,jcname16,jcpro16,jcscore16,jccancel16,jcfit16;
	
	
	JFrame JF17;
	JLabel studentsid17,courseid17,selectYear17,grade17;
	JTextField ssid17,cid17,sy17,g17;
	JButton iStuCoursesInfoa,iStuCoursesInfob;
	JPanel jssid17,jcid17,jsy17,jgrade17;
	
	
	public StudentWindow(){
		all = new JMenuBar();
		
		/*String path = "/Users/izayoi/Documents/workspace/DBMShomework/background.jpg"; 
		ImageIcon background = new ImageIcon(path); 
		JLabel label = new JLabel(background);
		label.setBounds(0, 0, this.getWidth(), this.getHeight());  
		JPanel imagePanel = (JPanel) this.getContentPane();
		imagePanel.setOpaque(false); 
		this.getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));  
        setVisible(true);  // 设置背景图片*/
       
		
		StudentInfo = new JMenu("学生信息");
		GradeInfo = new JMenu("成绩查询");
		CoursesInfo = new JMenu("课程信息");
		InsertInfo = new JMenu("增加信息");
		
		InsertStuInfo = new JMenuItem("增加学生信息");
		InsertCouInfo = new JMenuItem("增加课程信息");
		InsertSelInfo = new JMenuItem("增加选课信息");
		InsertStuInfo.addActionListener(this);
		InsertCouInfo.addActionListener(this);
		InsertSelInfo.addActionListener(this);
		InsertInfo.add(InsertStuInfo);
		InsertInfo.add(InsertCouInfo);
		InsertInfo.add(InsertSelInfo);
		
		stuBasicInfo = new JMenuItem("查询基本信息");
		stuCoursesInfo = new JMenuItem("查询选课情况");
		stuGPA = new JMenuItem("查询个人课程成绩");
		stuCourseGrade = new JMenuItem("查询个人加权");
		
		stuBasicInfo.addActionListener(this);
		stuCoursesInfo.addActionListener(this);
		stuGPA.addActionListener(this);
		stuCourseGrade.addActionListener(this);
		StudentInfo.add(stuBasicInfo);
		StudentInfo.add(stuCoursesInfo);
		StudentInfo.add(stuGPA);
		StudentInfo.add(stuCourseGrade);
		
		classGPA = new JMenuItem("查询班级成绩");
		courseGPA = new JMenuItem("查询课程平均成绩");
		classGPA.addActionListener(this);
		courseGPA.addActionListener(this);
		GradeInfo.add(classGPA);
		GradeInfo.add(courseGPA);
		
		couBasciInfo = new JMenuItem("查询课程基本信息");
		couSituation = new JMenuItem("查询选课情况");
		couBasciInfo.addActionListener(this);
		couSituation.addActionListener(this);
		CoursesInfo.add(couBasciInfo);
		CoursesInfo.add(couSituation);
				
		all.add(StudentInfo);
		all.add(CoursesInfo);
		all.add(GradeInfo);
		all.add(InsertInfo);
		
		female = new JButton("女");
		male = new JButton("男");
		male.addActionListener(this);
		female.addActionListener(this);
		
		this.setJMenuBar(all);
		this.setVisible(true);  
        this.setResizable(false);  
        this.setTitle("教务系统");  
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        this.setBounds(30, 30, 1000, 1000);  
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == stuBasicInfo){
			queryStuBasicInfo();
		}
		else if(e.getSource() == stuCoursesInfo){
			querystuCoursesInfo();
		}
		else if(e.getSource() == stuGPA){
			queryStuGPA();
		}
		else if(e.getSource() == stuCourseGrade){
			queryStuCourseGrade();
		}
		else if(e.getSource() == classGPA){
			queryClassGPA();
		}
		else if(e.getSource() == courseGPA){
			queryCourseGPA();
		}
		else if(e.getSource() == couBasciInfo){
			queryCouBasciInfo();
		}
		else if(e.getSource() == couSituation){
			queryCouSituation();
		}
		else if(e.getSource() == stuBasicInfoa){//11111
			SBIFA = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryStudentBasicInfo(sid.getText(), name.getText());
	        sid.setText("");
	        name.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        SBIFA.setVisible(true);  
	        SBIFA.setResizable(false);  
	        SBIFA.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        stuBasicInfod = new JButton("返回");
	        stuBasicInfod.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(stuBasicInfod);
	        
	        SBIFA.add(pane, BorderLayout.CENTER);
	        SBIFA.add(temp);
	        SBIFA.setLayout(new GridLayout(2,1));
	        SBIFA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        SBIFA.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == stuBasicInfob){
			UpdateStuBasicInfo();
		}
		else if(e.getSource() == stuBasicInfoc){
			JF.dispose();
		}
		else if(e.getSource() == stuBasicInfod){
			SBIFA.dispose();
		}
		else if(e.getSource() == stuBasicInfoe){
			deleteStuBasicInfo();
		}
		else if(e.getSource() == dstuBasicInfoa){//删除学生
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.DeleteStuBasicInfo(sid9.getText());
	        sid9.setText("");
		}
		else if(e.getSource() == dstuBasicInfob){
			JF9.dispose();
		}
		//11111
		else if(e.getSource() == stuCoursesInfoa){//2222222
			SCIF = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryStuCoursesInfo(sid2.getText(), name2.getText());
	        sid2.setText("");
	        name2.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        SCIF.setVisible(true);  
	        SCIF.setResizable(false);  
	        SCIF.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        stuCoursesInfod = new JButton("返回");
	        stuCoursesInfod.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(stuCoursesInfod);
	        
	        SCIF.add(pane, BorderLayout.CENTER);
	        SCIF.add(temp);
	        SCIF.setLayout(new GridLayout(2,1));
	        SCIF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        SCIF.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == stuCoursesInfob){
			UpdateStuCoursesInfo();
		}
		else if(e.getSource() == stuCoursesInfoc){
			JF2.dispose();
		}
		else if(e.getSource() == stuCoursesInfod){
			SCIF.dispose();
		}
		else if(e.getSource() == stuCoursesInfoe){
			deleteStuCoursesInfo();
		}
		else if(e.getSource() == dstuCoursesInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.DeleteStuCoursesInfo(sid11.getText(),cid11.getText());
	        sid11.setText("");
	        cid11.setText("");
		}
		else if(e.getSource() == dstuCoursesInfob){
			JF11.dispose();
		}
		else if(e.getSource() == stuGPAa){//33333
			SG = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryStuGPA(sid3.getText(), name3.getText(),cid1.getText(), cname1.getText());
	        sid3.setText("");
	        name3.setText("");
	        cid1.setText("");
	        cname1.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        SG.setVisible(true);  
	        SG.setResizable(false);  
	        SG.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        stuGPAd = new JButton("返回");
	        stuGPAd.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(stuGPAd);
	        
	        SG.add(pane, BorderLayout.CENTER);
	        SG.add(temp);
	        SG.setLayout(new GridLayout(2,1));
	        SG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        SG.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == stuGPAc){
			JF3.dispose();
		}
		else if(e.getSource() == stuGPAd){
			SG.dispose();
		}
		else if(e.getSource() == stuCourseGradea){//444444
			SCG = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryStuCourseGrade(sid4.getText());
	        sid4.setText("");
	        
	        //table.setToolTipText("显示结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        SCG.setVisible(true);  
	        SCG.setResizable(false);  
	        SCG.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        stuCourseGraded = new JButton("返回");
	        stuCourseGraded.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(stuCourseGraded);
	        
	        SCG.add(pane, BorderLayout.CENTER);
	        SCG.add(temp);
	        SCG.setLayout(new GridLayout(2,1));
	        SCG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        SCG.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == stuCourseGradec){//444444
			JF4.dispose();
		}
		else if(e.getSource() == stuCourseGraded){//444444
			SCG.dispose();
		}
		else if(e.getSource() == couBasciInfoa){//5555couBasciInfoa
			CBIF = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryCouBasciInfo(cidText5.getText(), cnameText5.getText());
	        cidText5.setText("");
	        cnameText5.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        CBIF.setVisible(true);  
	        CBIF.setResizable(false);  
	        CBIF.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        couBasciInfod = new JButton("返回");
	        couBasciInfod.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(couBasciInfod);
	        
	        CBIF.add(pane, BorderLayout.CENTER);
	        CBIF.add(temp);
	        CBIF.setLayout(new GridLayout(2,1));
	        CBIF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        CBIF.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == couBasciInfob){
			UpdateCouBasicInfo();
		}
		else if(e.getSource() == couBasciInfoc){
			JF5.dispose();
		}
		else if(e.getSource() == couBasciInfod){
			CBIF.dispose();
		}
		else if(e.getSource() == couBasciInfoe){
			deleteCouBasicInfo();
		}
		else if(e.getSource() == dcouBasicInfoa){//删除学生
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.DeleteCouBasicInfo(cid10.getText());
	        cid10.setText("");
		}
		else if(e.getSource() == dcouBasicInfob){
			JF10.dispose();
		}
		else if(e.getSource() == couSituationa){//6666couBasciInfoa
			CSA = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryCouSituation(cidText6.getText(), cnameText6.getText());
	        cidText6.setText("");
	        cnameText6.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        CSA.setVisible(true);  
	        CSA.setResizable(false);  
	        CSA.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        couSituationd = new JButton("返回");
	        couSituationd.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(couSituationd);
	        
	        CSA.add(pane, BorderLayout.CENTER);
	        CSA.add(temp);
	        CSA.setLayout(new GridLayout(2,1));
	        CSA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        CSA.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == couSituationc){
			JF6.dispose();
		}
		else if(e.getSource() == couSituationd){
			CSA.dispose();
		}
		else if(e.getSource() == courseGPAa){//77777courseGPAa
			CG = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryCourseGPA(cidText7.getText());
	        cidText7.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        CG.setVisible(true);  
	        CG.setResizable(false);  
	        CG.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        courseGPAc = new JButton("返回");
	        courseGPAc.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(courseGPAc);
	        
	        CG.add(pane, BorderLayout.CENTER);
	        CG.add(temp);
	        CG.setLayout(new GridLayout(2,1));
	        CG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        CG.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == courseGPAb){
			JF7.dispose();
		}
		else if(e.getSource() == courseGPAc){
			CG.dispose();
		}
		else if(e.getSource() == classGPAa){//8888classGPAa
			CG2 = new JFrame();
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        JTable table = QAUS.QueryClassGPA(classidText8.getText());
	        classidText8.setText("");
	        
	        table.setToolTipText("显示全部查询结果");
	        table.setCellSelectionEnabled(true); //设置单元格现在方式
	        table.setShowVerticalLines(true);  //设置是否显示单元格间的分隔线
	        
	        CG2.setVisible(true);  
	        CG2.setResizable(false);  
	        CG2.setTitle("结果");
	        
	        JScrollPane pane = new JScrollPane(table);
	        classGPAc = new JButton("返回");
	        classGPAc.addActionListener(this);
	        JPanel temp = new JPanel();
	        temp.add(classGPAc);
	        
	        CG2.add(pane, BorderLayout.CENTER);
	        CG2.add(temp);
	        CG2.setLayout(new GridLayout(2,1));
	        CG2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        CG2.setBounds(30, 30, 1000, 1000); 
		}
		else if(e.getSource() == classGPAb){
			JF8.dispose();
		}
		else if(e.getSource() == classGPAc){
			CG2.dispose();
		}
		else if(e.getSource() == female){
			flag = 2;
		}
		else if(e.getSource() == male){
			flag = 1;
		}
		else if(e.getSource() == uStuBasicInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.UpdateStuBasicInfo(sid12.getText(),sname12.getText(),flag,sage12.getText(),sregister12.getText(),sclass12.getText());
	        sid12.setText("");
	        sname12.setText("");
	        sage12.setText("");
	        sregister12.setText("");
	        sclass12.setText("");
	        flag = 0;
		}
		else if(e.getSource() == uStuBasicInfob){
			JF12.dispose();
		}
		else if(e.getSource() == uCouBasicInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.UpdateCouBasicInfo(cid13.getText(),cname13.getText(),cpro13.getText(),cscore13.getText(),ccancel13.getText(), cfit13.getText());
	        cid13.setText("");
	        cname13.setText("");
	        cpro13.setText("");
	        cscore13.setText("");
	        ccancel13.setText("");
	        cfit13.setText("");
		}
		else if(e.getSource() == uCouBasicInfob){
			JF13.dispose();
		}
		else if(e.getSource() == uStuCoursesInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.UpdateStuCoursesInfo(ssid14.getText(),cid14.getText(),sy14.getText(),g14.getText());
	        ssid14.setText("");
	        cid14.setText("");
	        sy14.setText("");
	        g14.setText("");
		}
		else if(e.getSource() == uStuCoursesInfob){
			JF14.dispose();
		}
		else if(e.getSource() == InsertStuInfo){
			InsertStudentInfo();
		}
		else if(e.getSource() == InsertCouInfo){
			InsertCoursesInfo();
		}
		else if(e.getSource() == InsertSelInfo){
			InsertSelectInfo();
		}
		else if(e.getSource() == iStuBasicInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.InsertStuBasicInfo(sid15.getText(),sname15.getText(),flag,sage15.getText(),sregister15.getText(),sclass15.getText());
	        sid15.setText("");
	        sname15.setText("");
	        sage15.setText("");
	        sregister15.setText("");
	        sclass15.setText("");
	        flag = 0;
		}
		else if(e.getSource() == iStuBasicInfob){
			JF15.dispose();
		}
		else if(e.getSource() == iCouBasicInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.InsertCouBasicInfo(cid16.getText(),cname16.getText(),cpro16.getText(),cscore16.getText(),ccancel16.getText(), cfit16.getText());
	        cid16.setText("");
	        cname16.setText("");
	        cpro16.setText("");
	        cscore16.setText("");
	        ccancel16.setText("");
	        cfit16.setText("");
		}
		else if(e.getSource() == iCouBasicInfob){
			JF16.dispose();
		}
		else if(e.getSource() == iStuCoursesInfoa){
			QueryAndUpdateStudent QAUS = new QueryAndUpdateStudent();
	        QAUS.ConnectSQL();
	        QAUS.InsertStuCoursesInfo(ssid17.getText(),cid17.getText(),sy17.getText(),g17.getText());
	        ssid17.setText("");
	        cid17.setText("");
	        sy17.setText("");
	        g17.setText("");
		}
		else if(e.getSource() == iStuCoursesInfob){
			JF17.dispose();
		}
	}

	private void InsertSelectInfo() {
		// TODO Auto-generated method stub
		System.out.println("17");
		JF17 = new JFrame();
		studentsid17 = new JLabel("请输入学号");
		courseid17 = new JLabel("请输入课程编号");
		selectYear17 = new JLabel("请选择选课年份");
		grade17 = new JLabel("请输入成绩(可为空)");
		
		ssid17 = new JTextField(10);
		cid17 = new JTextField(10);
		sy17 = new JTextField(10);
		g17 = new JTextField(10);
		
		iStuCoursesInfoa = new JButton("增加");
		iStuCoursesInfob = new JButton("返回");
		iStuCoursesInfoa.addActionListener(this);
		iStuCoursesInfob.addActionListener(this);
		jssid17 = new JPanel();
		jcid17 = new JPanel();
		jsy17 = new JPanel();
		jgrade17 = new JPanel();
		jssid17.add(studentsid17);
		jssid17.add(ssid17);
		jcid17.add(courseid17);
		jcid17.add(cid17);
		jsy17.add(selectYear17);
		jsy17.add(sy17);
		jgrade17.add(grade17);
		jgrade17.add(g17);
		JPanel sb = new JPanel();
		
		JF17.setVisible(true);  
        JF17.setResizable(false);  
        JF17.setTitle("请输入全部信息");
        JF17.add(jssid17);
        JF17.add(jcid17);
        JF17.add(jsy17);
        JF17.add(jgrade17);
        JF17.add(iStuCoursesInfoa);
        JF17.add(iStuCoursesInfob);
        sb.add(iStuCoursesInfoa);
        sb.add(iStuCoursesInfob);

        JF17.add(sb);
        JF17.setLayout(new GridLayout(7,1));
		JF17.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF17.setBounds(50,50,400, 400);
	}

	private void InsertCoursesInfo() {
		// TODO Auto-generated method stub
		System.out.println("16");
		JF16 = new JFrame();
		couCid16 = new JLabel("请输入课程编号");
		couName16 = new JLabel("请输入课程名称");
		couPro16 = new JLabel("请选择任课教师");
		couScore16 = new JLabel("请输入学分");
		couCancel16 = new JLabel("请输入取消年份(可为空)");
		couFit16 = new JLabel("请输入适合年级");
		
		cid16 = new JTextField(10);
		cname16 = new JTextField(10);
		cpro16 = new JTextField(10);
		cscore16 = new JTextField(10);
		ccancel16 = new JTextField(10);
		cfit16 = new JTextField(10);
		
		iCouBasicInfoa = new JButton("增加");
		iCouBasicInfob = new JButton("返回");
		iCouBasicInfoa.addActionListener(this);
		iCouBasicInfob.addActionListener(this);
		jcid16 = new JPanel();
		jcname16 = new JPanel();
		jcpro16 = new JPanel();
		jcscore16 = new JPanel();
		jccancel16 = new JPanel();
		jcfit16 = new JPanel();
		jcid16.add(couCid16);
		jcid16.add(cid16);
		jcname16.add(couName16);
		jcname16.add(cname16);
		jcpro16.add(couPro16);
		jcpro16.add(cpro16);
		jcscore16.add(couScore16);
		jcscore16.add(cscore16);
		jccancel16.add(couCancel16);
		jccancel16.add(ccancel16);
		jcfit16.add(couFit16);
		jcfit16.add(cfit16);
		JPanel sb = new JPanel();
		
		JF16.setVisible(true);  
        JF16.setResizable(false);  
        JF16.setTitle("请输入全部信息");
        JF16.add(jcid16);
        JF16.add(jcname16);
        JF16.add(jcpro16);
        JF16.add(jcscore16);
        JF16.add(jccancel16);
        JF16.add(jcfit16);
        JF16.add(iCouBasicInfoa);
        JF16.add(iCouBasicInfob);
        sb.add(iCouBasicInfoa);
        sb.add(iCouBasicInfob);

        JF16.add(sb);
        JF16.setLayout(new GridLayout(7,1));
		JF16.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF16.setBounds(50,50,400, 400);
	}

	private void InsertStudentInfo() {
		// TODO Auto-generated method stub
		System.out.println("12");
		JF15 = new JFrame();
		stuSid15 = new JLabel("请输入学号");
		stuName15 = new JLabel("请输入姓名");
		stuGender15 = new JLabel("请选择性别");
		stuAge15 = new JLabel("请输入年龄");
		stuRegister15 = new JLabel("请输入入学年份");
		stuClass15 = new JLabel("请输入班级");
		sid15 = new JTextField(10);
		sname15 = new JTextField(10);
		
		sage15 = new JTextField(10);
		sregister15 = new JTextField(10);
		sclass15 = new JTextField(10);
		iStuBasicInfoa = new JButton("增加");
		iStuBasicInfob = new JButton("返回");
		iStuBasicInfoa.addActionListener(this);
		iStuBasicInfob.addActionListener(this);
		jsid15 = new JPanel();
		jname15 = new JPanel();
		jgender15 = new JPanel();
		jage15 = new JPanel();
		jregister15 = new JPanel();
		jclass15 = new JPanel();
		jsid15.add(stuSid15);
		jsid15.add(sid15);
		jname15.add(stuName15);
		jname15.add(sname15);
		jgender15.add(stuGender15);
		jgender15.add(male);
		jgender15.add(female);
		jage15.add(stuAge15);
		jage15.add(sage15);
		jregister15.add(stuRegister15);
		jregister15.add(sregister15);
		jclass15.add(stuClass15);
		jclass15.add(sclass15);
		JPanel sb = new JPanel();
		
		JF15.setVisible(true);  
        JF15.setResizable(false);  
        JF15.setTitle("请输入全部信息");
        JF15.add(jsid15);
        JF15.add(jname15);
        JF15.add(jgender15);
        JF15.add(jage15);
        JF15.add(jregister15);
        JF15.add(jclass15);
        JF15.add(iStuBasicInfoa);
        JF15.add(iStuBasicInfob);
        JF15.add(iStuBasicInfoa);
        JF15.add(iStuBasicInfob);
        sb.add(iStuBasicInfoa);
        sb.add(iStuBasicInfob);
        sb.add(iStuBasicInfoa);
        sb.add(iStuBasicInfob);

        JF15.add(sb);
        JF15.setLayout(new GridLayout(7,1));
		JF15.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF15.setBounds(50,50,400, 400);
	}

	private void UpdateCouBasicInfo() {
		// TODO Auto-generated method stub
		System.out.println("13");
		JF13 = new JFrame();
		couCid13 = new JLabel("请输入课程编号");
		couName13 = new JLabel("请输入课程名称");
		couPro13 = new JLabel("请选择任课教师");
		couScore13 = new JLabel("请输入学分");
		couCancel13 = new JLabel("请输入取消年份(可为空)");
		couFit13 = new JLabel("请输入适合年级");
		
		cid13 = new JTextField(10);
		cname13 = new JTextField(10);
		cpro13 = new JTextField(10);
		cscore13 = new JTextField(10);
		ccancel13 = new JTextField(10);
		cfit13 = new JTextField(10);
		
		uCouBasicInfoa = new JButton("修改");
		uCouBasicInfob = new JButton("返回");
		uCouBasicInfoa.addActionListener(this);
		uCouBasicInfob.addActionListener(this);
		jcid13 = new JPanel();
		jcname13 = new JPanel();
		jcpro13 = new JPanel();
		jcscore13 = new JPanel();
		jccancel13 = new JPanel();
		jcfit13 = new JPanel();
		jcid13.add(couCid13);
		jcid13.add(cid13);
		jcname13.add(couName13);
		jcname13.add(cname13);
		jcpro13.add(couPro13);
		jcpro13.add(cpro13);
		jcscore13.add(couScore13);
		jcscore13.add(cscore13);
		jccancel13.add(couCancel13);
		jccancel13.add(ccancel13);
		jcfit13.add(couFit13);
		jcfit13.add(cfit13);
		JPanel sb = new JPanel();
		
		JF13.setVisible(true);  
        JF13.setResizable(false);  
        JF13.setTitle("请输入全部信息");
        JF13.add(jcid13);
        JF13.add(jcname13);
        JF13.add(jcpro13);
        JF13.add(jcscore13);
        JF13.add(jccancel13);
        JF13.add(jcfit13);
        JF13.add(uCouBasicInfoa);
        JF13.add(uCouBasicInfob);
        JF13.add(uCouBasicInfoa);
        JF13.add(uCouBasicInfob);
        sb.add(uCouBasicInfoa);
        sb.add(uCouBasicInfob);
        sb.add(uCouBasicInfoa);
        sb.add(uCouBasicInfob);

        JF13.add(sb);
        JF13.setLayout(new GridLayout(7,1));
		JF13.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF13.setBounds(50,50,400, 400);
	}

	private void UpdateStuCoursesInfo() {
		// TODO Auto-generated method stub
		System.out.println("13");
		JF14 = new JFrame();
		studentsid14 = new JLabel("请输入学号");
		courseid14 = new JLabel("请输入课程编号");
		selectYear14 = new JLabel("请选择选课年份");
		grade14 = new JLabel("请输入成绩(可为空)");
		
		ssid14 = new JTextField(10);
		cid14 = new JTextField(10);
		sy14 = new JTextField(10);
		g14 = new JTextField(10);
		
		uStuCoursesInfoa = new JButton("修改");
		uStuCoursesInfob = new JButton("返回");
		uStuCoursesInfoa.addActionListener(this);
		uStuCoursesInfob.addActionListener(this);
		jssid14 = new JPanel();
		jcid14 = new JPanel();
		jsy14 = new JPanel();
		jgrade14 = new JPanel();
		jssid14.add(studentsid14);
		jssid14.add(ssid14);
		jcid14.add(courseid14);
		jcid14.add(cid14);
		jsy14.add(selectYear14);
		jsy14.add(sy14);
		jgrade14.add(grade14);
		jgrade14.add(g14);
		JPanel sb = new JPanel();
		
		JF14.setVisible(true);  
        JF14.setResizable(false);  
        JF14.setTitle("请输入全部信息");
        JF14.add(jssid14);
        JF14.add(jcid14);
        JF14.add(jsy14);
        JF14.add(jgrade14);
        JF14.add(uStuCoursesInfoa);
        JF14.add(uStuCoursesInfob);
        sb.add(uStuCoursesInfoa);
        sb.add(uStuCoursesInfob);

        JF14.add(sb);
        JF14.setLayout(new GridLayout(7,1));
		JF14.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF14.setBounds(50,50,400, 400);
	}

	private void UpdateStuBasicInfo() {
		// TODO Auto-generated method stub
		System.out.println("12");
		JF12 = new JFrame();
		stuSid12 = new JLabel("请输入学号");
		stuName12 = new JLabel("请输入姓名");
		stuGender12 = new JLabel("请选择性别");
		stuAge12 = new JLabel("请输入年龄");
		stuRegister12 = new JLabel("请输入入学年份");
		stuClass12 = new JLabel("请输入班级");
		sid12 = new JTextField(10);
		sname12 = new JTextField(10);
		
		sage12 = new JTextField(10);
		sregister12 = new JTextField(10);
		sclass12 = new JTextField(10);
		uStuBasicInfoa = new JButton("修改");
		uStuBasicInfob = new JButton("返回");
		uStuBasicInfoa.addActionListener(this);
		uStuBasicInfob.addActionListener(this);
		jsid12 = new JPanel();
		jname12 = new JPanel();
		jgender12 = new JPanel();
		jage12 = new JPanel();
		jregister12 = new JPanel();
		jclass12 = new JPanel();
		jsid12.add(stuSid12);
		jsid12.add(sid12);
		jname12.add(stuName12);
		jname12.add(sname12);
		jgender12.add(stuGender12);
		jgender12.add(male);
		jgender12.add(female);
		jage12.add(stuAge12);
		jage12.add(sage12);
		jregister12.add(stuRegister12);
		jregister12.add(sregister12);
		jclass12.add(stuClass12);
		jclass12.add(sclass12);
		JPanel sb = new JPanel();
		
		JF12.setVisible(true);  
        JF12.setResizable(false);  
        JF12.setTitle("请输入全部信息");
        JF12.add(jsid12);
        JF12.add(jname12);
        JF12.add(jgender12);
        JF12.add(jage12);
        JF12.add(jregister12);
        JF12.add(jclass12);
        JF12.add(uStuBasicInfoa);
        JF12.add(uStuBasicInfob);
        JF12.add(uStuBasicInfoa);
        JF12.add(uStuBasicInfob);
        sb.add(uStuBasicInfoa);
        sb.add(uStuBasicInfob);
        sb.add(uStuBasicInfoa);
        sb.add(uStuBasicInfob);

        JF12.add(sb);
        JF12.setLayout(new GridLayout(7,1));
		JF12.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF12.setBounds(50,50,400, 400);
	}

	private void deleteStuCoursesInfo() {
		// TODO Auto-generated method stub
		JF11 = new JFrame();
		stuSid11 = new JLabel("请输入学号");
		couCid11 = new JLabel("请输入课程编号");
		sid11 = new JTextField(10);
		cid11 = new JTextField(10);
		dstuCoursesInfoa = new JButton("删除");
		dstuCoursesInfob = new JButton("返回");
		dstuCoursesInfoa.addActionListener(this);
		dstuCoursesInfob.addActionListener(this);
		jcid11 = new JPanel();
		jsid11 = new JPanel();
		jsid11.add(stuSid11);
		jsid11.add(sid11);
		jcid11.add(couCid11);
		jcid11.add(cid11);
		JPanel sb = new JPanel();
		
		JF11.setVisible(true);  
        JF11.setResizable(false);  
        JF11.setTitle("输入信息");
        JF11.add(jsid11);
        JF11.add(jcid11);
        JF11.add(dstuCoursesInfoa);
        JF11.add(dstuCoursesInfob);
        sb.add(dstuCoursesInfoa);
        sb.add(dstuCoursesInfob);

        JF11.add(sb);
        JF11.setLayout(new GridLayout(3,1));
		JF11.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF11.setBounds(50,50,400, 400); 
	}

	private void deleteCouBasicInfo() {
		// TODO Auto-generated method stub
		JF10 = new JFrame();
		couCid10 = new JLabel("请输入课程编号");
		cid10 = new JTextField(10);
		dcouBasicInfoa = new JButton("删除");
		dcouBasicInfob = new JButton("返回");
		dcouBasicInfoa.addActionListener(this);
		dcouBasicInfob.addActionListener(this);
		jcid10 = new JPanel();
		jcid10.add(couCid10);
		jcid10.add(cid10);
		JPanel sb = new JPanel();
		
		JF10.setVisible(true);  
        JF10.setResizable(false);  
        JF10.setTitle("输入信息");
        JF10.add(jcid10);
        JF10.add(dcouBasicInfoa);
        JF10.add(dcouBasicInfob);
        sb.add(dcouBasicInfoa);
        sb.add(dcouBasicInfob);

        JF10.add(sb);
        JF10.setLayout(new GridLayout(3,1));
		JF10.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF10.setBounds(50,50,400, 400); 
	}

	private void deleteStuBasicInfo() {
		// TODO Auto-generated method stub
		JF9 = new JFrame();
		stuSid9 = new JLabel("请输入学号");
		sid9 = new JTextField(10);
		dstuBasicInfoa = new JButton("删除");
		dstuBasicInfob = new JButton("返回");
		dstuBasicInfoa.addActionListener(this);
		dstuBasicInfob.addActionListener(this);
		jsid9 = new JPanel();
		jsid9.add(stuSid9);
		jsid9.add(sid9);
		JPanel sb = new JPanel();
		
		JF9.setVisible(true);  
        JF9.setResizable(false);  
        JF9.setTitle("输入信息");
        JF9.add(jsid9);
        JF9.add(dstuBasicInfoa);
        JF9.add(dstuBasicInfob);
        sb.add(dstuBasicInfoa);
        sb.add(dstuBasicInfob);

        JF9.add(sb);
        JF9.setLayout(new GridLayout(3,1));
		JF9.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF9.setBounds(50,50,400, 400); 
	}

	private void queryCouSituation() {
		// TODO Auto-generated method stub
		JF6 = new JFrame();
		cid6 = new JLabel("请输入课程编号");
		cname6 = new JLabel("请输入课程名称");
		cidText6 = new JTextField(10);
		cnameText6 = new JTextField(10);
		couSituationa = new JButton("查询");
		couSituationc = new JButton("返回");
		couSituationa.addActionListener(this);
		couSituationc.addActionListener(this);
		jcid6 = new JPanel();
		jcname6 = new JPanel();
		jcid6.add(cid6);
		jcid6.add(cidText6);
		jcname6.add(cname6);
		jcname6.add(cnameText6);
		JPanel sb = new JPanel();
		
		JF6.setVisible(true);  
        JF6.setResizable(false);  
        JF6.setTitle("输入信息");
        JF6.add(jcid6);
        JF6.add(jcname6);
        JF6.add(couSituationa);
        JF6.add(couSituationc);
        sb.add(couSituationa);
        sb.add(couSituationc);

        JF6.add(sb);
        JF6.setLayout(new GridLayout(3,1));
		JF6.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF6.setBounds(50,50,400, 400); 
	}

	private void queryCouBasciInfo() {
		// TODO Auto-generated method stub
		JF5 = new JFrame();
		cid5 = new JLabel("请输入课程编号");
		cname5 = new JLabel("请输入课程名称");
		cidText5 = new JTextField(10);
		cnameText5 = new JTextField(10);
		couBasciInfoa = new JButton("查询");
		couBasciInfob = new JButton("修改");
		couBasciInfoe = new JButton("删除");
		couBasciInfoc = new JButton("返回");
		couBasciInfoa.addActionListener(this);
		couBasciInfob.addActionListener(this);
		couBasciInfoe.addActionListener(this);
		couBasciInfoc.addActionListener(this);
		jcid5 = new JPanel();
		jcname5 = new JPanel();
		jcid5.add(cid5);
		jcid5.add(cidText5);
		jcname5.add(cname5);
		jcname5.add(cnameText5);
		JPanel sb = new JPanel();
		
		JF5.setVisible(true);  
        JF5.setResizable(false);  
        JF5.setTitle("输入信息");
        JF5.add(jcid5);
        JF5.add(jcname5);
        JF5.add(couBasciInfoa);
        JF5.add(couBasciInfob);
        JF5.add(couBasciInfoe);
        JF5.add(couBasciInfoc);
        sb.add(couBasciInfoa);
        sb.add(couBasciInfob);
        sb.add(couBasciInfoe);
        sb.add(couBasciInfoc);

        JF5.add(sb);
        JF5.setLayout(new GridLayout(3,1));
		JF5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF5.setBounds(50,50,400, 400); 
	}

	private void queryCourseGPA() {
		// TODO Auto-generated method stub
		JF7 = new JFrame();
		cid7 = new JLabel("请输入课程编号");
		cidText7 = new JTextField(10);
		courseGPAa = new JButton("查询");
		courseGPAb = new JButton("返回");
		courseGPAa.addActionListener(this);
		courseGPAb.addActionListener(this);
		jcid7 = new JPanel();
		jcid7.add(cid7);
		jcid7.add(cidText7);
		JPanel sb = new JPanel();
		
		JF7.setVisible(true);  
        JF7.setResizable(false);  
        JF7.setTitle("输入信息");
        JF7.add(jcid7);
        JF7.add(courseGPAa);
        JF7.add(courseGPAb);
        sb.add(courseGPAa);
        sb.add(courseGPAb);

        JF7.add(sb);
        JF7.setLayout(new GridLayout(3,1));
		JF7.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF7.setBounds(50,50,400, 400); 
	}

	private void queryClassGPA() {
		// TODO Auto-generated method stub
		JF8 = new JFrame();
		classid8 = new JLabel("请输入班级编号");
		classidText8 = new JTextField(10);
		classGPAa = new JButton("查询");
		classGPAb = new JButton("返回");
		classGPAa.addActionListener(this);
		classGPAb.addActionListener(this);
		jclassid7 = new JPanel();
		jclassid7.add(classid8);
		jclassid7.add(classidText8);
		JPanel sb = new JPanel();
		
		JF8.setVisible(true);  
        JF8.setResizable(false);  
        JF8.setTitle("输入信息");
        JF8.add(jclassid7);
        JF8.add(classGPAa);
        JF8.add(classGPAb);
        sb.add(classGPAa);
        sb.add(classGPAb);

        JF8.add(sb);
        JF8.setLayout(new GridLayout(3,1));
		JF8.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF8.setBounds(50,50,400, 400); 
	}

	public void queryStuCourseGrade() {
		// TODO Auto-generated method stub
		JF4 = new JFrame();
		stuSid4 = new JLabel("请输入学号");
		sid4 = new JTextField(10);
		stuCourseGradea = new JButton("查询");
		stuCourseGradec = new JButton("返回");
		stuCourseGradea.addActionListener(this);
		stuCourseGradec.addActionListener(this);
		jsid4 = new JPanel();
		jsid4.add(stuSid4);
		jsid4.add(sid4);
		
		JPanel sb = new JPanel();
		
		JF4.setVisible(true);  
        JF4.setResizable(false);  
        JF4.setTitle("输入信息");
        JF4.add(jsid4);
        
        JF4.add(stuCourseGradea);
        JF4.add(stuCourseGradec);
        sb.add(stuCourseGradea);
        sb.add(stuCourseGradec);

        JF4.add(sb);
        JF4.setLayout(new GridLayout(3,1));
		JF4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF4.setBounds(50,50,400, 400); 
	}
	
	public void queryStuGPA() {
		// TODO Auto-generated method stub
		JF3 = new JFrame();
		stuSid3 = new JLabel("请输入学号");
		stuName3 = new JLabel("请输入姓名");
		courseid = new JLabel("请输入课程编号");
		coursename = new JLabel("请输入课程名称");
		sid3 = new JTextField(10);
		name3 = new JTextField(10);
		cid1 = new JTextField(10);
		cname1 = new JTextField(10);
		stuGPAa = new JButton("查询");
		stuGPAc = new JButton("返回");
		stuGPAa.addActionListener(this);
		stuGPAc.addActionListener(this);
		jsid3 = new JPanel();
		jname3 = new JPanel();
		jcid1 = new JPanel();
		jcname1 = new JPanel();
		jsid3.add(stuSid3);
		jsid3.add(sid3);
		jname3.add(stuName3);
		jname3.add(name3);
		jcid1.add(courseid);
		jcid1.add(cid1);
		jcname1.add(coursename);
		jcname1.add(cname1);
		
		JPanel sb = new JPanel();
		
		JF3.setVisible(true);  
        JF3.setResizable(false);  
        JF3.setTitle("输入信息");
        JF3.add(jsid3);
        JF3.add(jname3);
        JF3.add(jcid1);
        JF3.add(jcname1);
        JF3.add(stuGPAa);
        JF3.add(stuGPAc);
        sb.add(stuGPAa);
        sb.add(stuGPAc);

        JF3.add(sb);
        JF3.setLayout(new GridLayout(5,1));
		JF3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF3.setBounds(50,50,400, 400); 
	}
	

	public void querystuCoursesInfo() {
		// TODO Auto-generated method stub
		JF2 = new JFrame();
		stuSid2 = new JLabel("请输入学号");
		stuName2 = new JLabel("请输入姓名");
		sid2 = new JTextField(10);
		name2 = new JTextField(10);
		stuCoursesInfoa = new JButton("查询");
		stuCoursesInfob = new JButton("修改");
		stuCoursesInfoe = new JButton("删除");
		stuCoursesInfoc = new JButton("返回");
		stuCoursesInfoa.addActionListener(this);
		stuCoursesInfob.addActionListener(this);
		stuCoursesInfoe.addActionListener(this);
		stuCoursesInfoc.addActionListener(this);
		jsid2 = new JPanel();
		jname2 = new JPanel();
		jsid2.add(stuSid2);
		jsid2.add(sid2);
		jname2.add(stuName2);
		jname2.add(name2);
		JPanel sb = new JPanel();
		
		JF2.setVisible(true);  
        JF2.setResizable(false);  
        JF2.setTitle("输入信息");
        JF2.add(jsid2);
        JF2.add(jname2);
        JF2.add(stuCoursesInfoa);
        JF2.add(stuCoursesInfob);
        JF2.add(stuCoursesInfoe);
        JF2.add(stuCoursesInfoc);
        sb.add(stuCoursesInfoa);
        sb.add(stuCoursesInfob);
        sb.add(stuCoursesInfoe);
        sb.add(stuCoursesInfoc);

        JF2.add(sb);
        JF2.setLayout(new GridLayout(3,1));
		JF2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF2.setBounds(50,50,400, 400); 
	}
	

	public void queryStuBasicInfo()  {
		// TODO Auto-generated method student
		//查询学生基本信息
		JF = new JFrame();
		stuSid = new JLabel("请输入学号");
		stuName = new JLabel("请输入姓名");
		sid = new JTextField(10);
		name = new JTextField(10);
		stuBasicInfoa = new JButton("查询");
		stuBasicInfob = new JButton("修改");
		stuBasicInfoe = new JButton("删除");
		stuBasicInfoc = new JButton("返回");
		stuBasicInfoa.addActionListener(this);
		stuBasicInfob.addActionListener(this);
		stuBasicInfoe.addActionListener(this);
		stuBasicInfoc.addActionListener(this);
		jsid = new JPanel();
		jname = new JPanel();
		jsid.add(stuSid);
		jsid.add(sid);
		jname.add(stuName);
		jname.add(name);
		JPanel sb = new JPanel();
		
		JF.setVisible(true);  
        JF.setResizable(false);  
        JF.setTitle("输入信息");
        JF.add(jsid);
        JF.add(jname);
        JF.add(stuBasicInfoa);
        JF.add(stuBasicInfob);
        JF.add(stuBasicInfoe);
        JF.add(stuBasicInfoc);
        sb.add(stuBasicInfoa);
        sb.add(stuBasicInfob);
        sb.add(stuBasicInfoe);
        sb.add(stuBasicInfoc);

        JF.add(sb);
        JF.setLayout(new GridLayout(3,1));
		JF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JF.setBounds(50,50,400, 400); 
        
	}
}
