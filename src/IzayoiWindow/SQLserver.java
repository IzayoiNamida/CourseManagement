package IzayoiWindow;

import java.sql.*;  

import javax.swing.JOptionPane;  

public class SQLserver {
	Connection ct;   
    PreparedStatement ps,ps2;  
    ResultSet rs,rs2;  
    String user,pwd;  
    //将连接数据库的方法封装为一个方法  
    public void ConnectSQL()  
    {  
        try {  
            Class.forName("com.mysql.jdbc.Driver"); //加载驱动  
            ct = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/student?user=root&password=709414&useSSL=false");
              
            System.out.println("已成功连接数据库...");  
              
        } catch (Exception e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        }  
    }  
      
    //注册用户的方法  
    public void UserRegis(String a,String b,String d,int flag)  
    {  
        //创建火箭车  
        try {  
        	if(flag == 1){
        		ps=ct.prepareStatement("insert into StudentsUsers values(?,?,?)");  
        	}
        	else if(flag == 2){
        		ps=ct.prepareStatement("insert into TeachersUsers values(?,?,?)"); 
        	}
            ps.setString(1,a);  
            ps.setString(2,d);  
            ps.setString(3,b);  
              
            //执行  
            int i=ps.executeUpdate();  
            if(i==1)  
            {  
                JOptionPane.showMessageDialog(null, "注册成功","提示消息",JOptionPane.WARNING_MESSAGE);  
                  
            }else  
            {  
                JOptionPane.showMessageDialog(null, "注册失败","提示消息",JOptionPane.ERROR_MESSAGE);  
            }  
              
              
        } 
        catch (SQLException e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        }  
    }  
      
//  登录验证方法  
    public void SQLverify(String a,String b, int flag)  
    {  
    	
    	if(flag == 0){
    		JOptionPane.showMessageDialog(null, "请选择登陆方式", "提示消息", JOptionPane.ERROR_MESSAGE);
    	}
    	else{
	    	try {  
	    		if(a == null || b == null){
	    			JOptionPane.showMessageDialog(null, "用户名或密码不可为空！", "提示消息", JOptionPane.ERROR_MESSAGE);
	    		}
	    		else{
		    		if(flag == 1){
		    			ps=ct.prepareStatement("select * from StudentsUsers where userSid= ? and stupsw=?"); 
		    		}
		    		else{
		    			ps=ct.prepareStatement("select * from TeachersUsers where userTid=? and teapsw=? "); 
		    		} 
		            ps.setString(1, a);  
		            ps.setString(2, b);  
		            // ResultSet结果集,大家可以把ResultSet理解成返回一张表行的结果集  
		            rs = ps.executeQuery();  
		           
		            if(rs.next())  
		            {  
		                user = rs.getString(1);  
		                pwd = rs.getString(2);  
		                if(flag == 1)
		                {
		                	new TeacherWindow();
		                }
		                else 
		                {
		                	new StudentWindow();
		                }
		            }
		            else  
		            {  
		                JOptionPane.showMessageDialog(null, "用户名或者密码错误，请重新输入！", "提示消息", JOptionPane.ERROR_MESSAGE); 
		            }  
	            }
	              
	        } catch (SQLException e) {  
	              
	            e.printStackTrace();  
	        }  
    	}
    }  
      
    //注册验证方法，判断用户名是否已经存在  
    public void ZhuceVerify(String a, int flag,String b, String c)  
    {  
        try {  
        	if(flag == 1){
        		ps=ct.prepareStatement("select * from StudentsUsers where userSid= ?");   
        	}
        	else if(flag == 2){
        		ps=ct.prepareStatement("select * from TeachersUsers where userTid =?"); 
        	}
            ps.setString(1, a);               
            rs=ps.executeQuery();  
            if(rs.next())  
            {  
                JOptionPane.showMessageDialog(null, "该用户已经存在", "提示信息", JOptionPane.WARNING_MESSAGE);  
            }
            else  
            {  
            	//进行注册  
                //studentRegister ui= new studentRegister(flag);  
            	if(flag == 1){
	            	ps2 = ct.prepareStatement("select * from Students where sid= ?");
	            	ps2.setString(1, a);               
	                rs2=ps.executeQuery();
	                if(rs2.next()){
	                	this.UserRegis(a,b,c,flag);  
	                }
	                else{
	                	JOptionPane.showMessageDialog(null, "未查询到该学生", "提示信息", JOptionPane.WARNING_MESSAGE);  
	                }
            	}
            	else{
            		this.UserRegis(a,b,c,flag);  
            	}
            }  
              
        } catch (SQLException e) {  
              
            e.printStackTrace();  
        }  
    }  
}
